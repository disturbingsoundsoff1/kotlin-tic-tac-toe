import java.awt.BasicStroke
import java.awt.Color
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.event.ActionListener
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import javax.swing.JPanel
import javax.swing.Timer

val tableX = 100
val tableY = 100
var mX = 0
var mY = 0
val table = mutableListOf(
    mutableListOf(0, 0, 0),
    mutableListOf(0, 0, 0),
    mutableListOf(0, 0, 0),
)
lateinit var timer: Timer

class Panel : JPanel() {

    init {
        background = Color.BLACK
        val mListener = MListener()
        addMouseListener(mListener)
        timer = Timer(300, ActionListener {
            repaint()
        })
        timer.start()
    }

    override fun paint(gr: Graphics?) {
        super.paint(gr)
        val g: Graphics2D = (gr as Graphics2D?)!!

        g.color = Color.WHITE
        g.stroke = BasicStroke(5F)
        for (i in 0..300 step 100) {
            g.drawLine(i + tableX, 0 + tableY, i + tableX, 300 + tableY)
            g.drawLine(0 + tableX, i + tableY, 300 + tableX, i + tableY)
        }

        for (x in 0 until table.size) {

            for (y in 0 until table[x].size) {
                print("${table[x][y]}, ")
                when (table[x][y]) {
                    1 -> drawX(g, x, y)
                    2 -> drawO(g, x, y)
                    else -> continue
                }

                if (table[x][y] == 1) {
                    drawX(g, x, y)
                }
            }
            println()
        }
        println()
    }

    private fun drawO(g: Graphics2D, x: Int, y: Int) {
        g.color = Color(152, 251, 152)
        g.drawOval(x * 100 + tableX + 10, y * 100 + tableY + 10, 80, 80)
    }

    private fun drawX(g: Graphics2D, x: Int, y: Int) {
        g.color = Color(210, 100, 102)
        g.drawLine(
            x * 100 + tableX + 20, y * 100 + tableY + 20,
            x * 100 + 100 + tableX - 20, y * 100 + 100 + tableY - 20
        )
        g.drawLine(
            x * 100 + 100 + tableX - 20, y * 100 + tableY + 20,
            x * 100 + tableX + 20, y * 100 + 100 + tableY - 20
        )
        repaint()
    }

}


class MListener : MouseListener {
    var playerCheck = false
    override fun mouseClicked(p0: MouseEvent?) {
        if (p0 != null) {
            mX = p0.x
            mY = p0.y
            playerCheck = playerTick()
            //checkWin()
            if (playerCheck && !stop()) {
                botTick()
                //checkWin()
            }

        }
    }

    override fun mousePressed(p0: MouseEvent?) {

    }

    override fun mouseReleased(p0: MouseEvent?) {

    }

    override fun mouseEntered(p0: MouseEvent?) {

    }

    override fun mouseExited(p0: MouseEvent?) {

    }

    private fun playerTick(): Boolean {

        if (mX > tableX && mX < tableX + 300
            && mY > tableY && mY < tableY + 300
        ) {
            val x = (mX - tableX) / 100
            val y = (mY - tableY) / 100
            if (table[x][y] == 0) {
                table[x][y] = 1
                return true
            } else return false
        } else return false
    }

    private fun botTick() {
        var check = false
        while (!check) {
            val x = (0..2).random()
            val y = (0..2).random()
            if (table[x][y] == 0) {
                table[x][y] = 2
                check = true
            }
        }
        playerCheck = false
    }

    private fun stop(): Boolean {
        var stop = true
        for (i in table) {
            if (i.contains(0)) stop = false
        }
        return stop
    }

    private fun checkWin() {
        TODO("CHECK WIN")
    }
}